# Generated by Django 5.0.4 on 2024-04-23 18:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("committees", "0005_committee_description"),
    ]

    operations = [
        migrations.AlterField(
            model_name="committee",
            name="banner",
            field=models.ImageField(
                default="defaults/event_banner.webp",
                upload_to="committees/",
                validators=[
                    django.core.validators.FileExtensionValidator(
                        allowed_extensions=["jpg", "jpeg", "png", "gif", "webp"]
                    )
                ],
                verbose_name="Banner",
            ),
        ),
    ]
