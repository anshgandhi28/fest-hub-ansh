from django.urls import path, include
from . import views
from django.contrib import admin

app_name = 'events'

urlpatterns= [
    path('', views.events, name='events'),
    path('<uuid:uuid>', views.event, name='event'),
    path('form/', views.events_form, name='events_form'),
    path('pin_event/', views.pin_event, name='pin_event'), 
    path('unpin_event/', views.unpin_event, name='unpin_event'),
    path('register_event/', views.register_event, name='register_event'),
    path('registerations/', views.registrations, name='registrations'),
]